package org.mian.gitnex.structs;

public interface BottomSheetListener {
    void onButtonClicked(String text);
}
